use crate::input::InputAction;
use pancurses::Input;
use std::collections::HashMap;

pub fn generic() -> HashMap<Input, InputAction> {
    let mut map = HashMap::new();
    map.insert(Input::Character('q'), InputAction::Quit);
    map
}
pub fn dvorak_vim() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character('t'), InputAction::MoveNorth);
    map.insert(Input::Character('n'), InputAction::MoveEast);
    map.insert(Input::Character('h'), InputAction::MoveSouth);
    map.insert(Input::Character('d'), InputAction::MoveWest);
    map
}
pub fn dvorak_vim_homepos() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character('n'), InputAction::MoveNorth);
    map.insert(Input::Character('s'), InputAction::MoveEast);
    map.insert(Input::Character('t'), InputAction::MoveSouth);
    map.insert(Input::Character('h'), InputAction::MoveWest);
    map
}
pub fn dvorak_aswd() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character(','), InputAction::MoveNorth);
    map.insert(Input::Character('e'), InputAction::MoveEast);
    map.insert(Input::Character('o'), InputAction::MoveSouth);
    map.insert(Input::Character('a'), InputAction::MoveWest);
    map
}
pub fn dvorak_aswd_homepos() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character('.'), InputAction::MoveNorth);
    map.insert(Input::Character('u'), InputAction::MoveEast);
    map.insert(Input::Character('e'), InputAction::MoveSouth);
    map.insert(Input::Character('o'), InputAction::MoveWest);
    map
}
pub fn qwerty_vim() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character('k'), InputAction::MoveNorth);
    map.insert(Input::Character('l'), InputAction::MoveEast);
    map.insert(Input::Character('j'), InputAction::MoveSouth);
    map.insert(Input::Character('h'), InputAction::MoveWest);
    map
}
pub fn qwerty_vim_homepos() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character('l'), InputAction::MoveNorth);
    map.insert(Input::Character(';'), InputAction::MoveEast);
    map.insert(Input::Character('k'), InputAction::MoveSouth);
    map.insert(Input::Character('j'), InputAction::MoveWest);
    map
}
pub fn qwerty_aswd() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character('w'), InputAction::MoveNorth);
    map.insert(Input::Character('d'), InputAction::MoveEast);
    map.insert(Input::Character('s'), InputAction::MoveSouth);
    map.insert(Input::Character('a'), InputAction::MoveWest);
    map
}
pub fn qwerty_aswd_homepos() -> HashMap<Input, InputAction> {
    let mut map = generic();
    map.insert(Input::Character('q'), InputAction::Quit);
    map.insert(Input::Character('e'), InputAction::MoveNorth);
    map.insert(Input::Character('f'), InputAction::MoveEast);
    map.insert(Input::Character('d'), InputAction::MoveSouth);
    map.insert(Input::Character('s'), InputAction::MoveWest);
    map
}
