// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! (Middleware) This module contains the `Scheduler` and `Task` types.

use std::{
    cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd},
    collections::BinaryHeap,
    time::{Duration, Instant},
};

pub const DURATION_ZERO: Duration = Duration::from_secs(0);

#[derive(Clone, Debug)]
/// `Task` implements the std::cmp traits, but the implementations only care about how overdue a
/// task is.
pub struct Task<V> {
    /// This is supplied by the user.
    value: V,
    /// If the task is recurring, this is the 'cooldown' until it's run again.
    recurring: Option<Duration>,
    /// When the task is due or `None` if the task is done.
    due: Option<Instant>,
    /// When this task was created. This is used to uniquely identify tasks.
    created: Instant,
}

#[derive(Clone, Debug)]
pub struct CompareByOverdue<V>(pub Task<V>);

#[derive(Clone, Debug, Default)]
pub struct Scheduler<V> {
    /// Priority queue which stores the tasks
    heap: BinaryHeap<CompareByOverdue<V>>,
}

impl<V> Task<V> {
    /// Create a new `Task`.
    pub fn new_once(value: V, due: Instant) -> Self {
        Self {
            value,
            recurring: None,
            due: Some(due),
            created: Instant::now(),
        }
    }
    /// Create a new recurring `Task`.
    pub fn new_recur(value: V, cooldown: Duration) -> Self {
        Self {
            value,
            due: Some(Instant::now() + cooldown),
            recurring: Some(cooldown),
            created: Instant::now(),
        }
    }
    /// Create a new `Task` which is due at the given instant and recurrs after that.
    pub fn new_recur_at(value: V, due: Instant, cooldown: Duration) -> Self {
        Self {
            value,
            due: Some(due),
            recurring: Some(cooldown),
            created: Instant::now(),
        }
    }
    /// Immutable access to this task's value.
    pub fn value(&self) -> &V {
        &self.value
    }
    /// Mutable access to this task's value.
    pub fn value_mut(&mut self) -> &mut V {
        &mut self.value
    }
    /// Returns an immutable reference to the `due` field of this task.
    pub fn due(&self) -> &Option<Instant> {
        &self.due
    }
    /// Returns `true` when the task is due.
    pub fn is_due(&self) -> bool {
        match self.due {
            Some(due) => due < Instant::now(),
            None => false,
        }
    }
    /// Returns `true` when the task is done.
    pub fn is_done(&self) -> bool {
        self.due.is_none()
    }
    /// Mark the task as done, updating when the task is next due
    pub fn done(&mut self) {
        match self.recurring {
            Some(cooldown) => self.due = Some(Instant::now() + cooldown),
            None => self.due = None,
        }
    }
}

impl<V> PartialEq for CompareByOverdue<V> {
    fn eq(&self, other: &Self) -> bool {
        self.0.due == other.0.due
    }
}

impl<V> Eq for CompareByOverdue<V> {}

impl<V> PartialOrd for CompareByOverdue<V> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl<V> Ord for CompareByOverdue<V> {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self.0.due, other.0.due) {
            // we order by how long the tasks are overdue, so smaller values are greater here
            (Some(a), Some(b)) => b.cmp(&a),
            (Some(_), None) => Ordering::Greater,
            (None, Some(_)) => Ordering::Less,
            (None, None) => Ordering::Equal,
        }
    }
}

impl<V> Scheduler<V> {
    /// Create a new, empty `Scheduler`
    pub fn new() -> Self {
        Self {
            heap: BinaryHeap::new(),
        }
    }
    /// Adds a task to the scheduler
    pub fn add_task(&mut self, task: Task<V>) {
        self.heap.push(CompareByOverdue(task))
    }
    /// Returns `true` if there is a due task
    pub fn has_due(&self) -> bool {
        // alternatively: self.peek().is_some()
        if let Some(wrapper) = self.heap.peek() {
            wrapper.0.is_due()
        } else {
            false
        }
    }
    /// Returns an immutable reference to the most overdue task, if there is one.
    pub fn peek(&self) -> Option<&Task<V>> {
        let task = &self.heap.peek()?.0;
        if task.is_due() {
            Some(task)
        } else {
            None
        }
    }
    /// Returns the most over due task, if ther is one.
    pub fn pop(&mut self) -> Option<Task<V>> {
        if self.heap.peek()?.0.is_due() {
            Some(self.heap.pop().unwrap().0)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn compare() {
        let t0 = CompareByOverdue(Task::new_once((), Instant::now()));
        let t1 = CompareByOverdue(Task::new_once((), Instant::now()));
        assert!(t0 == t0);
        assert!(t1 == t1);
        assert!(t0 >= t1);
        assert!(t1 <= t0);
    }
}
