// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! (Client) Types and functions for dealing with different keyboard layouts.

use pancurses::Input;
use std::collections::HashMap;

pub mod keymaps;

/// This enum contains variants for all the things that can be controlled by user input. It
/// represents a keystroke and servers as an abstraction layer for different keyboard layouts.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum InputAction {
    Quit,
    MoveNorth,
    MoveEast,
    MoveSouth,
    MoveWest,
}

pub type FnKeymap = fn() -> HashMap<Input, InputAction>;

pub fn keymap_list() -> HashMap<&'static str, FnKeymap> {
    use self::keymaps::*;
    let mut map = HashMap::new();
    map.insert("dvorak_vim", dvorak_vim as FnKeymap);
    map.insert("dvorak_vim_homepos", dvorak_vim_homepos as FnKeymap);
    map.insert("dvorak_aswd", dvorak_aswd as FnKeymap);
    map.insert("dvorak_aswd_homepos", dvorak_aswd_homepos as FnKeymap);
    map.insert("qwerty_vim", qwerty_vim as FnKeymap);
    map.insert("qwerty_vim_homepos", qwerty_vim_homepos as FnKeymap);
    map.insert("qwerty_aswd", qwerty_aswd as FnKeymap);
    map.insert("qwerty_aswd_homepos", qwerty_aswd_homepos as FnKeymap);
    map
}
