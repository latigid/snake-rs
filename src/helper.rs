// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! (Middleware) This module contains helper functions.

/// substract `change` from `num`. when reaching `0`, wrap around to `max`
pub fn wrapped_sub(num: u32, change: u32, max: u32) -> u32 {
    if change <= num {
        num - change
    } else {
        max - change + num
    }
}

/// add `change` to `num`. when reaching `max`, wrap around to `0`.
pub fn wrapped_add(num: u32, change: u32, max: u32) -> u32 {
    if let Some(s) = num.checked_add(change) {
        if s >= max {
            s - max
        } else {
            s
        }
    } else {
        num - (max - change)
    }
}
