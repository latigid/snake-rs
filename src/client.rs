// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! (Client) This module contains the client struct and related types.

use self::ClientAction::*;
use crate::{
    game::{ArenaAction::*, Direction::*, *},
    input::InputAction,
    scheduler::*,
};
use pancurses::{Input, Window};
use std::{collections::HashMap, thread::sleep, time::Duration};

pub const TIME_BETWEEN_FRAMES: Duration = Duration::from_millis(42);

/// This enum contains variants for all the public facing things the client can do
#[derive(Clone, Debug)]
pub enum ClientAction {
    /// Tell the arena to do something
    ArenaDo(ArenaAction),
    /// Render and draw a frame
    Draw,
    // /// Remove the oldest status message
    //PopStatus,
}

pub struct SnakeClient {
    arena: Arena,
    center_on: Position,
    scheduler: Scheduler<ClientAction>,
    snake_focus: Option<u32>,
    keymap: HashMap<Input, InputAction>,
    wmain: Window,
    warena: Window,
}

impl SnakeClient {
    /// Create a new [`SnakeClient`] which displays and controlls snakes in the [`Arena`] `arena`.
    /// `snake_focus` is the snake that the client will focus on and control.
    ///
    /// # Errors
    ///
    /// * if `snake_focus` is none and does not contain a snake index the arena knows about,
    /// [`GameError::UndefinedSnake`] is returned.
    ///
    /// [`GameError::UndefinedSnake`]: ../game/enum.GameError.html#variant.UnknownSnake
    ///
    /// # Example
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction, GameResult, Position};
    /// use snake_rs::client::SnakeClient;
    ///
    /// fn main() -> GameResult<()> {
    ///     let mut arena = Arena::new(Position::new(32, 32));

    ///     Ok(())
    /// }
    /// ```
    ///
    pub fn new(
        arena: Arena,
        keymap: HashMap<Input, InputAction>,
        snake_focus: Option<u32>,
    ) -> GameResult<Self> {
        let center_on = match snake_focus {
            Some(si) => *arena.snake(si)?.head(),
            None => Position::new(0, 0),
        };
        let wmain = pancurses::initscr();
        Ok(Self {
            center_on,
            snake_focus,
            arena,
            scheduler: Scheduler::new(),
            warena: wmain.dupwin(),
            wmain,
            keymap,
        })
    }
    /// This method set's up the terminal, then runs the main loop. After the main loop exits, it
    /// restores the terminal and returns the main loop's result.
    ///
    /// # Errors
    ///
    // TODO see below lines
    /// Currently the main loop isn't handling most of the errors and just returns when it
    /// encounters one. The best way to handle errors is to log them and abort. This behaviour will
    /// change in the future.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use snake_rs::client::SnakeClient;
    /// use snake_rs::game::{Arena, Direction, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     // Create an arena with 32x32 cells
    ///     let mut arena = Arena::new(Position::new(31, 31));
    ///
    ///     // Add a snake at (4, 4) facing west with 4 segments
    ///     let s0 = arena.add_snake(Position::new(4, 4), Direction::West)?;
    ///     arena.grow_snake(s0, 3)?;
    ///     // Add a snake at (7, 5) facing east with 5 segments
    ///     let s1 = arena.add_snake(Position::new(7, 5), Direction::East)?;
    ///     arena.grow_snake(s1, 4)?;
    ///
    ///     // Create the client
    ///     let mut client = SnakeClient::new(arena, Some(s0))?;
    ///
    ///     // run the main loop until the user quits the game or an error occurs
    ///     client.run()?;
    ///
    ///     Ok(())
    /// }
    /// ```
    ///
    /// # Intern
    ///
    /// This method is just a wrapper around [`SnakeClient::run_intern`] and takes care of setting
    /// up the terminal, calling the actual main loop and restoring the terminal afterwards.
    ///
    pub fn run(&mut self) -> GameResult<()> {
        pancurses::noecho();
        pancurses::curs_set(0);
        self.wmain.nodelay(true);
        let result = self.run_intern();
        pancurses::echo();
        pancurses::curs_set(1);
        result
    }
    // See documentation of [`SnakeClient::run()`].
    fn run_intern(&mut self) -> GameResult<()> {
        let keys = self.arena.snakes().keys().cloned().collect::<Vec<_>>();
        for snake in keys {
            self.scheduler.add_task(Task::new_recur(
                ArenaDo(MoveSnake(snake)),
                Duration::from_millis(320),
            ));
        }
        self.scheduler
            .add_task(Task::new_recur(Draw, Duration::from_millis(50)));
        'main: loop {
            while let Some(key) = self.wmain.getch() {
                use crate::input::InputAction::*;
                match self.keymap.get(&key) {
                    Some(Quit) => break 'main,
                    Some(other) => {
                        self.handle_key(*other)?;
                    }
                    None => (),
                }
            }
            self.handle_tasks()?;
            sleep(TIME_BETWEEN_FRAMES);
        }
        Ok(())
    }
    fn draw(&self) -> GameResult<()> {
        use crate::graphics::*;
        let mut canvas = self.cells_in_screen();
        // adjust for border. we only need to decrement by one, as one cell has two characters
        canvas.x -= 1;
        // adjust for border and infobar
        canvas.y -= 3;
        let [before, after, window, offset] = self.calculate_positions_draw_arena(canvas);
        // print the top part of the border
        self.warena.mvprintw(
            0,
            0,
            format!("┌{}┐", "──".repeat(canvas.x as usize)),
        );
        // draw the OOB area above the window
        for _ in 0..before.y {
            self.warena
                .printw(format!("│{}│", TILE_OOB.repeat(canvas.x as usize)));
        }
        // draw the field line by line
        for y in offset.y..window.y + offset.y {
            self.warena
                .printw(format!("│{}", TILE_OOB.repeat(before.x as usize)));
            for x in offset.x..window.x + offset.x {
                if y >= self.arena.size().y || x >= self.arena.size().x {
                    self.warena.printw("??");
                    continue;
                }
                let pos = &&Position::new(x, y);
                self.warena
                    .printw(if let Some((si, kind)) = self.arena.map().get(pos) {
                        match kind {
                            SegmentKind::Head => match self.arena.snake(*si)?.dir() {
                                North => SNAKE_HEAD_NORTH,
                                East => SNAKE_HEAD_EAST,
                                South => SNAKE_HEAD_SOUTH,
                                West => SNAKE_HEAD_WEST,
                            },
                            SegmentKind::Body => SNAKE_BODY,
                            SegmentKind::Tail => match self.arena.snake(*si)?.dir() {
                                North | South => SNAKE_TAIL_NORTH,
                                East | West => SNAKE_TAIL_EAST,
                            },
                        }
                    } else {
                        TILE_EMPTY
                    });
            }
            self.warena
                .printw(format!("{}│", TILE_OOB.repeat(after.x as usize)));
        }
        // draw the OOB area after the window
        for _ in 0..after.y {
            self.warena
                .printw(format!("│{}│", TILE_OOB.repeat(canvas.x as usize)));
        }
        // draw the bottom part of the border
        self.warena
            .printw(format!("└{}┘", "──".repeat(canvas.x as usize)));
        self.warena
            .printw(format!("x:{} y:{}", self.center_on.x, self.center_on.y));
        self.warena.refresh();
        Ok(())
    }

    pub fn handle_tasks(&mut self) -> GameResult<()> {
        'tasks: loop {
            if !self.scheduler.has_due() {
                break 'tasks;
            }
            let mut task = self.scheduler.pop().unwrap();
            task.done();
            match task.value() {
                ArenaDo(aa) => match self.arena.handle_action(aa) {
                    Ok(()) => {
                        if let Some(si) = self.snake_focus {
                            if aa.might_move_snake(si) {
                                match self.arena.snake(si) {
                                    Err(GameError::UnknownSnake) => {
                                        self.snake_focus = None;
                                        continue 'tasks;
                                    },
                                    Err(e) => unreachable!("this error should never be encountered in this context: {:?}", e),
                                    Ok(snake) => self.center_on = *snake.head(),
                                }
                            }
                        }
                    }
                    Err(GameError::UnknownSnake) => continue 'tasks,
                    e => return e,
                },
                Draw => self.draw()?,
                //PopStatus => self.messages.pop(),
            }
            if !task.is_done() {
                self.scheduler.add_task(task)
            }
        }
        Ok(())
    }

    pub fn handle_key(&mut self, key: InputAction) -> GameResult<Option<InputAction>> {
        use crate::input::InputAction::*;
        if let Some(si) = self.snake_focus {
            match key {
                MoveNorth => drop(self.arena.turn_snake(si, North)),
                MoveEast => drop(self.arena.turn_snake(si, East)),
                MoveSouth => drop(self.arena.turn_snake(si, South)),
                MoveWest => drop(self.arena.turn_snake(si, West)),
                k => return Ok(Some(k)),
            }
            return Ok(None);
        }
        Ok(Some(key))
    }
    /// This function taskes the size of an area `size` and the size of a window `window` of said area
    /// may be larger, smaller or exactly the same size as said area.
    /// this function then calculates:
    /// * the space between the start of the window and the start of the area
    /// * the space between the end of the window and the end of the area
    /// * the size of the area seen through the window
    /// * the space before the area and the window
    ///
    /// This method takes the size of a drawable canvas and then calculates:
    ///  * the size of the shown OOB area before the window to the arena
    ///  * the size of the shown OOB area after the window to the arena
    ///  * the size of the window to the arena
    ///  * the point closest to (0, 0) of the arena show in the window.
    fn calculate_sizes_draw_arena(canvas: u32, arena: u32, center_on: u32) -> [u32; 4] {
        let half = canvas / 2;
        // calculate the size of the shown OOB area before the window to the arena
        let before = half.saturating_sub(center_on);
        // calculate the size of the shown OOB area after the window to the arena
        let mut after = half.saturating_sub(arena.saturating_sub(center_on));
        // calculate the size of the window to the arena
        let mut window = canvas - before - after;
        // if arena is larger that the arena's size, then adjust before and after. also pay
        // attention to make them equal if possible
        if window > arena {
            after += window - arena;
            window = arena;
        }
        // calculate the position closest to 0 of the arena that is shown in the window
        let offset = center_on.saturating_sub(half);
        [before, after, window, offset]
    }
    /// Convenience wrapper around [`SnakeClient::calculate_sizes_draw_arena`]. This wrapper takes
    /// the x and y values and puts them in [`Position`]s. All these values are for drawing the
    /// arena on the given canvas.
    fn calculate_positions_draw_arena(&self, canvas: Position) -> [Position; 4] {
        let x = Self::calculate_sizes_draw_arena(canvas.x, self.arena.size().x, self.center_on.x);
        let y = Self::calculate_sizes_draw_arena(canvas.y, self.arena.size().y, self.center_on.y);
        [
            Position::new(x[0], y[0]),
            Position::new(x[1], y[1]),
            Position::new(x[2], y[2]),
            Position::new(x[3], y[3]),
        ]
    }
    /// Returns the position of the cell in the lower right corner of the screen.
    fn cells_in_screen(&self) -> Position {
        let (height, width) = self.wmain.get_max_yx();
        let width = width as u32 / 2;
        let height = height as u32;
        Position::new(width, height)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::*;
    #[test]
    fn calculate_sizes_draw_arena_large_screen() {
        let arena = 32;
        let reality = SnakeClient::calculate_sizes_draw_arena(arena * 3, arena, 0);
        println!("{:?}", reality);
        assert_eq!(reality[0], 48, "before is wrong");
        assert_eq!(reality[1], 16, "after is wrong");
        assert_eq!(reality[2], arena, "window is wrong");
        assert_eq!(reality[3], 0, "offset is wrong");
        assert_eq!(
            reality.iter().take(3).sum::<u32>(),
            arena * 3,
            "sum is wrong"
        );
    }
    #[test]
    fn calculate_sizes_draw_arena_medium_screen() {
        let arena = 32;
        let reality = SnakeClient::calculate_sizes_draw_arena(arena * 2, arena, 0);
        println!("{:?}", reality);
        assert_eq!(reality[0], 32, "before is wrong");
        assert_eq!(reality[1], 0, "after is wrong");
        assert_eq!(reality[2], arena, "window is wrong");
        assert_eq!(reality[3], 0, "offset is wrong");
        assert_eq!(reality.iter().sum::<u32>(), arena * 2, "sum is wrong");
    }
    #[test]
    fn calculate_sizes_draw_arena_small_screen() {
        let arena = 32;
        let reality = SnakeClient::calculate_sizes_draw_arena(arena, arena, 0);
        println!("{:?}", reality);
        assert_eq!(reality[0], 16, "before is wrong");
        assert_eq!(reality[1], 0, "after is wrong");
        assert_eq!(reality[2], arena / 2, "window is wrong");
        assert_eq!(reality[3], 0, "offset is wrong");
        assert_eq!(reality.iter().sum::<u32>(), arena, "sum is wrong");
    }
    #[test]
    fn calculate_sizes_draw_arena_larger_screen_and_offset() {
        let arena = 32;
        let canvas = arena * 2;
        let offset = canvas / 2 + 5;
        let reality = SnakeClient::calculate_sizes_draw_arena(canvas, arena, offset);
        println!("{:?}", reality);
        assert_eq!(reality[0], 0, "before is wrong");
        assert_eq!(reality[1], 32, "after is wrong");
        assert_eq!(reality[2], arena, "window is wrong");
        assert_eq!(reality[3], 5, "offset is wrong");
        assert_eq!(reality.iter().take(3).sum::<u32>(), canvas, "sum is wrong");
    }
    #[test]
    fn calculate_sizes_draw_arena_smaller_screen_and_offset() {
        let arena = 32;
        let canvas = arena / 2;
        let offset = canvas + 4;
        let reality = SnakeClient::calculate_sizes_draw_arena(canvas, arena, offset);
        println!("{:?}", reality);
        assert_eq!(reality[0], 0, "before is wrong");
        assert_eq!(reality[1], 0, "after is wrong");
        assert_eq!(reality[2], canvas, "window is wrong");
        assert_eq!(reality[3], 12, "offset is wrong");
        assert_eq!(reality.iter().take(3).sum::<u32>(), canvas, "sum is wrong");
    }
    //#[test]
    //fn calculate_sizes_draw_arena_large_screen() {
    //let arena = 32;
    //let canvas = arena*3;
    //let offset = 0;
    //let reality = SnakeClient::calculate_sizes_draw_arena(canvas, arena, offset);
    //println!("{:?}", reality);
    //assert_eq!(reality[0], 48, "before is wrong");
    //assert_eq!(reality[1], 16, "after is wrong");
    //assert_eq!(reality[2], arena, "window is wrong");
    //assert_eq!(reality[3], 0, "offset is wrong");
    //assert_eq!(reality.iter().take(3).sum::<u32>(), canvas, "sum is wrong");
    //}
}
