// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! (Client) This module contains all the ascii sprite constants.

//const SNAKE_TAIL_NORTH: &str = "⣾⣷";
//const SNAKE_TAIL_EAST: &str = "⣿⡷";
//const SNAKE_TAIL_SOUTH: &str = "⢿ ";
//const SNAKE_TAIL_WEST: &str = "⢾⣿";

pub const SNAKE_TAIL_NORTH: &str = "⢸⡇";
pub const SNAKE_TAIL_EAST: &str = "⠶⠶";
pub const SNAKE_TAIL_SOUTH: &str = "⢸⡇";
pub const SNAKE_TAIL_WEST: &str = "⠶⠶";

// other ideas:  "⣞⣳ ⣪⣕ ⣮⣵ ⢮⡵ ⣺⣗";
//const SNAKE_HEAD_NORTH: &str = "⣪⣕";
//const SNAKE_HEAD_EAST: &str = "⡷⡱";
//const SNAKE_HEAD_SOUTH: &str = "⢝⡫";
//const SNAKE_HEAD_WEST: &str = "⢎⢾";

pub const SNAKE_HEAD_NORTH: &str = "⣮⣵";
pub const SNAKE_HEAD_EAST: &str = "⣿⡱";
pub const SNAKE_HEAD_SOUTH: &str = "⢟⡻";
pub const SNAKE_HEAD_WEST: &str = "⢎⣿";

pub const SNAKE_BODY: &str = "⣿⣿";
pub const SNAKE_GROWING: &str = "!!";
/// Empty tiles.
pub const TILE_EMPTY: &str = "  ";
/// Tiles within the view but outside the arena.
pub const TILE_OOB: &str = "░░"; // ░░ ▒▒ ▓▓ ▞▞
