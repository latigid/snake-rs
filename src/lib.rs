// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! A multiplayer snake/slither server and unicode art client.
//!
//! Use `cargo run` to run the client.

//pub mod ansi_keycodes;
pub mod client;
pub mod game;
pub mod graphics;
pub mod helper;
pub mod input;
pub mod scheduler;

/// Import your wished keyboard mapping here, the rest of the game will look here for the key
/// constants.
mod keymapping {}
