// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! (Middleware) This module contains types related to the game's logic.

use self::{ArenaAction::*, Direction::*, GameError::*};
use crate::helper::*;
use std::{collections::HashMap, ops::Not};

/// Marks a position on the screen or in the arena. TODO remove dependency on nalgebra, we barely
/// use their functionality.
pub type Position = nalgebra::Point2<u32>;

/// The Result returned by many functions and methods.
pub type GameResult<O> = Result<O, GameError>;

/// Used to specify an absolute direction. North is towards the top of the screen, East is towards
/// the left of the screen, South is towards the bottom of the screen and West is towards the
/// right of the screen.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

/// The different kinds of Snake segments. This enum is used in the arena's internal map.
#[derive(Copy, Clone, Debug, Hash, PartialEq)]
pub enum SegmentKind {
    Head,
    Body,
    Tail,
}

/// The error type commonly returned. It contains variants for all the things that can go wrong and
/// do not cause a panic.
#[derive(Clone, Debug, Hash, PartialEq)]
pub enum GameError {
    /// returned when the specified snake doesn't exist
    UnknownSnake,
    /// returned when trying to place something on an already occupied position
    PositionOccupied,
    /// Returned when trying to add a snake outside of the arena.
    OutOfBounds,
    /// Returned when an action would kill the snake in a context where you don't want to kill the
    /// snake.
    WouldKillSnake,
    /// A custom error message.
    Message(String),
}

/// A (de)serializeable representation of actions an [`Arena`] can perform by being passed to
/// [`Arena::handle_action`]. See [`Arena::handle_action`] for more information.
#[derive(Clone, Debug, Hash)]
pub enum ArenaAction {
    MoveSnake(u32),
    MoveSnakeTo(u32, Position),
}

/// The arena represents the playing field and implements the bulk of the games logic. Use
/// [`SnakeClient`] to display it.
///
/// [`SnakeClient`]: ../client/struct.SnakeClient.html
///
/// # Example
/// ```
/// use snake_rs::game::{Arena, Direction, GameError, GameResult, Position};
/// fn main() -> GameResult<()> {
///     // Create an arena with 33x33 cells.
///     let mut arena = Arena::new(Position::new(32, 32));
///
///     // Add a snake at (4, 4) facing east.
///     let snake0 = arena.add_snake(Position::new(4, 4), Direction::East)?;
///     // Add a snake at (6, 4) facing north.
///     let snake1 = arena.add_snake(Position::new(6, 4), Direction::North)?;
///
///     // Grow both snakes by 6 segments.
///     arena.grow_snake(snake0, 6);
///     arena.grow_snake(snake1, 6);
///
///     // We can update snake0's position.
///     // [`Arena::move_snake`] returns `GameResult(bool)`, where bool indicates whether the snake is still alive.
///     assert!(arena.move_snake(snake0)?);
///
///     // Now snake0 collides with snake1 and dies.
///     assert_eq!(false, arena.move_snake(snake0)?);
///
///     // there is only one snake in the arena now.
///     assert_eq!(1, arena.snakes().keys().len());
///
///     // If we attempt to uptade snake0's position again, we get an `GameError::UnknownSnake`.
///     match arena.move_snake(snake0) {
///         Err(GameError::UnknownSnake) => (), // rip
///         _ => unreachable!("in this example we know this is unreachable"),
///     }
///     Ok(())
/// }
/// ```
///
#[derive(Clone, Debug)]
pub struct Arena {
    snakes: HashMap<u32, Snake>,
    map: HashMap<Position, (u32, SegmentKind)>,
    free_indices: Vec<u32>,
    size: Position,
}

/// The `Snake` struct represents a snake and has convenience functions for accessing them.
#[derive(Clone, Debug, Hash)]
pub struct Snake {
    segments: Vec<Position>,
    dir: Direction,
}

impl Direction {
    /// return direction rotated by - 90 degree
    pub fn left(self) -> Self {
        match self {
            North => West,
            East => North,
            South => East,
            West => South,
        }
    }
    /// return direction rotated by 90 degree
    pub fn right(self) -> Self {
        match self {
            North => East,
            East => South,
            South => West,
            West => North,
        }
    }
}

impl Not for Direction {
    type Output = Direction;
    fn not(self) -> Self::Output {
        match self {
            North => South,
            East => West,
            South => North,
            West => East,
        }
    }
}

impl ArenaAction {
    /// Returns true if this action can affect the position of the specified snake. In other words,
    /// returns false if this action will not change the position of the snake. This is useful when
    /// you need to update something everytime a snake's position changed.
    pub(crate) fn might_move_snake(&self, given_si: u32) -> bool {
        match self {
            MoveSnake(action_si) | MoveSnakeTo(action_si, _) => *action_si == given_si,
            //    _ => false,
        }
    }
}

impl Arena {
    /// Creates a new Arena, where size is point furthest away from (0, 0) still included in the
    /// arena. In other words, the width of the arena will be `size.x + 1` and the height will be
    /// `size.y + 1`.
    pub fn new(size: Position) -> Self {
        Self {
            snakes: HashMap::new(),
            map: HashMap::new(),
            free_indices: Vec::new(),
            size,
        }
    }
    /// Creates a new snake and adds it to the arena, then returns the snake index.
    ///
    /// # Errors
    ///
    /// * If the specified position is outside the arena, [`GameError::OutOfBounds`] is returned.
    /// * If the specified position is already occupied by another snake or an obstacle (not
    /// implemented yet), [`GameError::PositionOccupied`] is returned.
    ///
    pub fn add_snake(&mut self, pos: Position, dir: Direction) -> Result<u32, GameError> {
        // error if the position is out of bounds
        if pos.x > self.size.x && pos.y > self.size.y {
            return Err(OutOfBounds);
        }
        // error if the position is aleady occupied
        if self.map.get(&pos).is_some() {
            return Err(PositionOccupied);
        }
        let si = self.get_next_index();
        let snake = Snake::new(pos, dir);
        self.snakes.insert(si, snake);
        // place the snake on the map
        self.map.insert(pos, (si, SegmentKind::Head));
        Ok(si)
    }
    /// Returns a new unique index to be used as key for the `Arena.snakes` map. In the future,
    /// u32 indices will be deprecated in favor of usernames or userids.
    fn get_next_index(&mut self) -> u32 {
        if let Some(index) = self.free_indices.pop() {
            return index;
        }
        self.snakes.len() as u32
    }
    /// Returns an immutable reference to the internal map of the field. This [`HashMap`] contains
    /// entries for all cells on which something is placed. Currently this can only be a snake, but
    /// in the future this may be food or other things. The value of the map is a tuple with the
    /// snake index and the type of segment on this cell.
    pub fn map(&self) -> &HashMap<Position, (u32, SegmentKind)> {
        &self.map
    }
    /// Returns an immutable reference to a [`HashMap`] containing all living snakes on the field.
    /// The key is the snake index, the value the snake object.
    pub fn snakes(&self) -> &HashMap<u32, Snake> {
        &self.snakes
    }
    /// Returns an immutable reference to the snake belonging to the given snake index.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction::*, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     // Create an arena with 33x33 cells.
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///
    ///     // Add a snake at (4, 4) facing north
    ///     let snake0 = arena.add_snake(Position::new(4, 4), North)?;
    ///
    ///     // print the position of the snake's head
    ///     println!("snake0's head is at: {}", arena.snake(snake0)?.head());
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn snake(&self, si: u32) -> GameResult<&Snake> {
        if let Some(snake) = self.snakes.get(&si) {
            Ok(snake)
        } else {
            Err(UnknownSnake)
        }
    }
    /// Returns a mutable reference to the snake belonging to the given snake index.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// see test `test_arena_snake_mut` in the same file
    fn snake_mut(&mut self, si: u32) -> GameResult<&mut Snake> {
        if let Some(snake) = self.snakes.get_mut(&si) {
            Ok(snake)
        } else {
            Err(UnknownSnake)
        }
    }
    /// Returns the position of the cell that is furthest away from (0, 0).
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Position};
    ///
    /// let pos = Position::new(32, 32);
    /// let arena = Arena::new(pos);
    /// assert_eq!(pos, arena.size());
    /// ```
    pub fn size(&self) -> Position {
        Position::new(self.size.x, self.size.y)
    }
    /// Moves the specified snake one cell forward in the direction the snake is facing.
    ///
    /// # Returns
    ///
    /// `GameResult<bool>` where the bool indicates whether the snake is still alive.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction::*, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///     let snake0 = arena.add_snake(Position::new(4, 4), East)?;
    ///
    ///     // the snake's head is at (4, 4)
    ///     assert_eq!(arena.snake(snake0)?.head(), &Position::new(4, 4));
    ///
    ///     // move the snake forward
    ///     assert!(arena.move_snake(snake0)?);
    ///
    ///     // the snake's head is at (5, 4)
    ///     assert_eq!(arena.snake(snake0)?.head(), &Position::new(5, 4));
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn move_snake(&mut self, si: u32) -> GameResult<bool> {
        let next_head = self.snake_next_pos(si)?;
        self.move_snake_to(si, next_head)
    }
    /// Moves the specified snake forward but place the head at the specified position.
    ///
    /// # Returns
    ///
    /// `GameResult<bool>` where the bool indicates whether the snake is still alive.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction::*, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///     let snake0 = arena.add_snake(Position::new(4, 4), East)?;
    ///
    ///     // the snake's head is at (4, 4)
    ///     assert_eq!(arena.snake(snake0)?.head(), &Position::new(4, 4));
    ///
    ///     // move the snake forward
    ///     assert!(arena.move_snake_to(snake0, Position::new(16, 16))?);
    ///
    ///     // the snake's head is at (5, 4)
    ///     assert_eq!(arena.snake(snake0)?.head(), &Position::new(16, 16));
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn move_snake_to(&mut self, si: u32, next: Position) -> GameResult<bool> {
        if self.map.get(&next).is_some() {
            // death
            // TODO figure out smart way to mark indices as available only when they are not at
            // the end
            let snake;
            if let Some(s) = self.snakes.remove(&si) {
                snake = s;
            } else {
                return Err(GameError::UnknownSnake);
            }
            self.free_indices.push(si);
            for pos in snake.segments() {
                let _ = self.map.remove(&pos);
            }
            Ok(false)
        } else {
            let old_head = *self.snake(si)?.head();
            // old head is now a  body
            self.map.get_mut(&old_head).unwrap().1 = SegmentKind::Body;
            let snake = self.snake_mut(si)?;
            // move the snake and delete the old tail from the map if the new tail isn't still on
            // that position
            let old_tail = snake.move_to_pos(next);
            if &old_tail != snake.tail() {
                self.map.remove(&old_tail);
            }
            let tail = *self.snake(si)?.tail();
            if tail != next {
                self.map.get_mut(&tail).unwrap().1 = SegmentKind::Tail;
            }
            self.map.insert(next, (si, SegmentKind::Head));
            Ok(true)
        }
    }
    /// Calculates and returns where the next position of the specified snake's head will be.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction::*, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///     let snake0 = arena.add_snake(Position::new(4, 4), East)?;
    ///
    ///     // the snake's head will  be at (5, 4)
    ///     assert_eq!(arena.snake_next_pos(snake0)?, Position::new(5, 4));
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn snake_next_pos(&mut self, si: u32) -> GameResult<Position> {
        let snake = self.snake(si)?;
        let mut head = *snake.head();
        match snake.dir {
            North => head.y = wrapped_sub(head.y, 1, self.size.y),
            East => head.x = wrapped_add(head.x, 1, self.size.x),
            South => head.y = wrapped_add(head.y, 1, self.size.y),
            West => head.x = wrapped_sub(head.x, 1, self.size.x),
        }
        Ok(head)
    }
    /// Shrinks the snake by removing it's tail.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    /// * If the snake has only a single segment, [`GameError::WouldKillSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction::*, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///     let snake0 = arena.add_snake(Position::new(4, 4), East)?;
    ///
    ///     // grow  snake0 by 6 segments
    ///     arena.grow_snake(snake0, 6)?;
    ///
    ///     // snake0 has 7 segments now
    ///     assert_eq!(arena.snake(snake0)?.segments().len(), 7);
    ///
    ///     // shring snake0
    ///     arena.shrink_snake(snake0)?;
    ///
    ///     // snake0 has 6 segments now
    ///     assert_eq!(arena.snake(snake0)?.segments().len(), 6);
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn shrink_snake(&mut self, si: u32) -> GameResult<Position> {
        let snake = self.snake_mut(si)?;
        if let Some(last_pos) = snake.shrink() {
            if snake.tail() != &last_pos {
                let _ = self
                    .map
                    .remove(&last_pos)
                    .expect("snake has an element that isn't on the map");
            }
            Ok(last_pos)
        } else {
            Err(GameError::WouldKillSnake)
        }
    }
    /// Grow the specified snake by duplicating it's tail.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction::*, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///     let snake0 = arena.add_snake(Position::new(4, 4), East)?;
    ///
    ///     // grow  snake0 by 6 segments
    ///     arena.grow_snake(snake0, 6)?;
    ///
    ///     // snake0 has 7 segments now
    ///     assert_eq!(arena.snake(snake0)?.segments().len(), 7);
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn grow_snake(&mut self, si: u32, segments: u32) -> GameResult<()> {
        self.snake_mut(si)?.grow(segments);
        Ok(())
    }
    /// Change the direction of the specified snake.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Direction::*, GameError, GameResult, Position};
    ///
    /// fn main() -> GameResult<()> {
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///     let snake0 = arena.add_snake(Position::new(4, 4), East)?;
    ///
    ///     // grow  snake0 by 6 segments
    ///     arena.turn_snake(snake0, North)?;
    ///
    ///     // snake0 has 7 segments now
    ///     assert_eq!(arena.snake(snake0)?.dir(), &North);
    ///
    ///     // we can't turn the snake South now, as it would slither into itself at the next
    ///     // update
    ///     match arena.turn_snake(snake0, South) {
    ///         Err(GameError::WouldKillSnake) => (),
    ///         _ => unreachable!(),
    ///     }
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn turn_snake(&mut self, si: u32, dir: Direction) -> GameResult<()> {
        self.snake_mut(si)?.turn(dir)
    }
    /// Change the direction of the specified snake.
    ///
    /// # Errors
    ///
    /// * If no snake belongs to the given snake index, [`GameError::UnknownSnake`] is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Arena, Position, Direction, ArenaAction, GameResult};
    /// fn main() -> GameResult<()> {
    ///     // Create an arena with 33x33 cells.
    ///     let mut arena = Arena::new(Position::new(32, 32));
    ///     // Add a snake to the arena and keep it's index.
    ///     let snake_index = arena.add_snake(Position::new(4, 4), Direction::East)?;
    ///
    ///     // Now we can controll the arena using `ArenaActions`
    ///     assert_eq!(arena.snake(snake_index)?.head(), &Position::new(4, 4));
    ///
    ///     arena.handle_action(&ArenaAction::MoveSnake(snake_index));
    ///     assert_eq!(arena.snake(snake_index)?.head(), &Position::new(5, 4));
    ///
    ///     arena.handle_action(&ArenaAction::MoveSnakeTo(snake_index, Position::new(8, 8)));
    ///     assert_eq!(arena.snake(snake_index)?.head(), &Position::new(8, 8));
    ///     Ok(())
    /// }
    /// ```
    ///
    pub fn handle_action(&mut self, action: &ArenaAction) -> GameResult<()> {
        match *action {
            MoveSnake(si) => self.move_snake(si)?,
            MoveSnakeTo(si, pos) => self.move_snake_to(si, pos)?,
        };
        Ok(())
    }
}

impl Snake {
    /// Create a new snake with a single segment `pos` and facing `dir`. To create a snake and add
    /// it to an [`Arena`] use [`Arena::add_snake`]. To grow snakes use [`Snake::grow`] or
    /// [`Arena::grow_snake`].
    ///
    /// # Examples
    ///
    /// ```
    /// use snake_rs::game::{Direction::*, Position, Snake};
    /// let mut snake = Snake::new(Position::new(4, 4), East);
    ///
    /// // grow the snake by 4 segments
    /// snake.grow(4);
    ///
    /// // the snake has 5 segments now
    /// assert_eq!(snake.segments().len(), 5);
    ///
    /// // shrink the snake by two segments
    /// snake.shrink();
    /// snake.shrink();
    ///
    /// // the snake has 3 segments now
    /// assert_eq!(snake.segments().len(), 3);
    ///
    /// // move the snake east four times
    /// for i in 1..5 {
    ///     snake.move_to_pos(Position::new(4 + i, 4));
    /// }
    ///
    /// // the snake's head is at (8, 4) now
    /// assert_eq!(snake.head(), &Position::new(8, 4));
    ///
    /// // the snake's tail is at (6, 4) now
    /// assert_eq!(snake.tail(), &Position::new(6, 4));
    /// ```
    ///
    pub fn new(pos: Position, dir: Direction) -> Self {
        Snake {
            segments: vec![pos],
            dir,
        }
    }
    /// Return an immutable reference to the head of the snake. This cannot fail, as every snake
    /// must have at least one segment

    pub fn head(&self) -> &Position {
        self.segments.first().expect("snakes must not be empty")
    }
    /// Return an immutable reference to the tail of the snake. This cannot fail, as every snake
    /// must have at least one segment. If the snake has only one segment, this is the same as the
    /// head.
    ///
    pub fn tail(&self) -> &Position {
        self.segments.last().expect("snakes must not be empty")
    }
    /// Return all the segments that are not head or tail. If the snake doesn't have more than 2
    /// segments, this is empty.
    pub fn body(&self) -> &[Position] {
        &self.segments[1..self.segments.len()]
    }
    /// Return all the segments of the snake. There must be at least one.
    pub fn segments(&self) -> &[Position] {
        &self.segments
    }
    /// Returs a reference to the direction of the snake's head.
    pub fn dir(&self) -> &Direction {
        &self.dir
    }
    /// If the snake has more than one segment, remove and return the last segment, otherwise
    /// return `None`.
    pub fn shrink(&mut self) -> Option<Position> {
        if self.segments.len() > 1 {
            self.segments.pop()
        } else {
            None
        }
    }
    /// Grow the snake by duplicating it's tail. This means there will be multiple segments in one
    /// place.
    pub fn grow(&mut self, segments: u32) {
        for _ in 0..segments {
            self.segments.push(*self.tail());
        }
    }
    // 90 degree kelvin, obviously
    /// Rotate the head of the snake by 90 degree
    pub fn turn_right(&mut self) {
        self.dir = self.dir.right();
    }
    /// Rotate the head of the snake by -90 degree
    pub fn turn_left(&mut self) {
        self.dir = self.dir.left();
    }
    /// Sets the snake's direction. Returns [`GameError::WouldKillSnake`] if the direction is the
    /// opposite of the current direction, as the snake would slither into itself and die.
    pub fn turn(&mut self, dir: Direction) -> GameResult<()> {
        if self.dir == !dir {
            Err(WouldKillSnake)
        } else {
            self.dir = dir;
            Ok(())
        }
    }
    /// Add `pos` as the new head of the snake, then remove and return the old tail.
    pub fn move_to_pos(&mut self, pos: Position) -> Position {
        self.segments.insert(0, pos);
        self.shrink()
            .expect("snakes must not be empty and we just grew it!")
    }
}

#[cfg(test)]
mod tests {
    use super::{Arena, Direction::*, GameResult, Position};
    #[test]
    fn arena_snake_mut() -> GameResult<()> {
        // Create an arena with 33x33 cells.
        let mut arena = Arena::new(Position::new(32, 32));

        // Add a snake at (4, 4) facing north
        let snake0 = arena.add_snake(Position::new(4, 4), North)?;

        // change the direction of the snake
        arena.snake_mut(snake0)?.turn_left();

        Ok(())
    }
}
