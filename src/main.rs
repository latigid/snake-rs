// Copyright 2018 digital
// This file is part of snake-rs.
//
// snake-rs is free software: you can redistribute it and/or modify it under
// the terms of the Affero GNU General Public License as published by the Free
// Software Foundation, either version 3 ofthe License, or (at your option)
// any later version.
//
// snake-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the Affero GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with snake-rs.  If not, see <https://www.gnu.org/licenses/>.

//! A multiplayer snake/slither server and unicode art client.
//!
//! Use `cargo +nightly run` to run the client (requires a nightly toolchain).

use argparse::{ArgumentParser, Store};
use snake_rs::{
    client::SnakeClient,
    game::{Direction::*, *},
    input::keymap_list,
};

fn main() -> Result<(), GameError> {
    let mut arena_width = 128;
    let mut arena_height = 128;
    let mut keymap_name = String::from("qwerty_aswd");
    let keymap;
    // let mut keymap = snake_rs::input::keymaps::dvorak_vim_homepos();
    let keymaplist = keymap_list();

    {
        // this block limits scope of borrows by ap.refer() method
        let mut ap = ArgumentParser::new();
        ap.set_description("Greet somebody.");
        ap.refer(&mut arena_width).add_option(
            &["-w", "--arena-width"],
            Store,
            "The width of the arena",
            );
        ap.refer(&mut arena_height).add_option(
            &["-h", "--arena-height"],
            Store,
            "The height of the arena",
            );
        ap.refer(&mut keymap_name)
            .add_option(&["-k", "--keymap"], Store, "The keymap to be used");
        ap.parse_args_or_exit();
    }

    if let Some(keymapfn) = keymaplist.get(&keymap_name.as_str()) {
        keymap = keymapfn();
    } else {
        println!("unknown keymap. possible keymaps are:");
        let mut layouts = keymaplist.keys().collect::<Vec<_>>();
        layouts.sort();
        for k in layouts {
            println!(" {}", k);
        }
        return Ok(());
    }
    // */
    let mut arena = Arena::new(Position::new(arena_width, arena_height));
    let s0 = arena.add_snake(Position::new(4, 4), East)?;
    let s1 = arena.add_snake(Position::new(16, 4), South)?;
    arena.grow_snake(s0, 64)?;
    arena.grow_snake(s1, 64)?;
    println!("hi");
    let mut client = SnakeClient::new(arena, keymap, Some(s0))?;
    client.run()?;
    println!("bye");
    Ok(())
}
